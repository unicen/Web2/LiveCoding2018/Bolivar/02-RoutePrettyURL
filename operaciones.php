<?php
  
  function sumar($arr) {
    $sum = 0;
    foreach ($arr as $value) {
      $sum += $value;
    }
    return $sum;
  }

  function restar($o1, $o2) {
    $resultado = $o1 - $o2;
    return $resultado;
  }

  function multiplicar($o1, $o2) {
    $resultado = $o1 * $o2;
    return $resultado;
  }


?>
