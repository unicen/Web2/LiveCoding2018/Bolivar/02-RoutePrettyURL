<?php
require_once 'home.php';
require_once 'operaciones.php';
require_once 'pi.php';
require_once 'about.php';

// constantes
define('ACTION', 0); 
define('VALOR1', 1);
define('VALOR2', 2);

// si no indica "action" forzamos asi entra al default
if (!isset($_GET['action']))
    $_GET['action'] = "home";

// parsea (separa) la url (si viene "sumar/5/8" => [sumar, 5, 8])
$action = $_GET['action'];
$partesURL = explode("/", $action);

// decide que acción tomar en base a la url
switch ($partesURL[ACTION]) {
    case 'about': 
        echo about($partesURL[VALOR1]);
        break;
    case 'pi':
        echo piNumber();
        break;
    case 'sumar':
        unset($partesURL[0]);
        echo sumar($partesURL);
        break;
    case 'restar':
        echo restar($partesURL[VALOR1],$partesURL[VALOR2]);
        break;
    case 'multiplicar':
        echo multiplicar($partesURL[VALOR1],$partesURL[VALOR2]);
        break;
    default: 
        echo home();
        break;
}
?>